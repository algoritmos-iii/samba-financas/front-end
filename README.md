# Trabalho para Disciplina de Algoritmos e Programação III


## Objetivo

* O projeto tem como principal objetivo de ajudar na organização das finanças do 
usuário, tendo como controlar despesas, receitas e até mesmo criar algumas 
metas a serem alcançadas.


## Requisitos básicos para poder utiliza-lo:

* Para utilizar o App, é necessário um celular com o sistema operacional Android.

## Guia de instalação

* Basta fazer o download o APK e seguir os passos de instalação, aceitando os 
termos de segurança do sistema.


## Tecnologias utilizadas no projeto:

* ReactNative

## Lincença
Licença MIT

Direitos autorais (c) 2019 Samba Finanças

É concedida permissão, gratuitamente, a qualquer pessoa que obtenha uma cópia deste software e dos arquivos de documentação associados (o "Software"), para lidar com o Software sem restrições, incluindo, sem limitação, os direitos de uso, cópia, modificação e fusão , publicar, distribuir, sublicenciar e / ou vender cópias do Software e permitir que pessoas a quem o Software é fornecido o façam, sujeitas às seguintes condições:

O aviso de copyright acima e este aviso de permissão devem ser incluídos em todas as cópias ou partes substanciais do Software.

O SOFTWARE É FORNECIDO "NO ESTADO EM QUE SE ENCONTRA", SEM NENHUM TIPO DE GARANTIA, EXPRESSA OU IMPLÍCITA, 
INCLUINDO, MAS NÃO SE LIMITANDO ÀS GARANTIAS DE COMERCIALIZAÇÃO, ADEQUAÇÃO A UM FIM ESPECÍFICO E NÃO VIOLAÇÃO. 
EM NENHUMA CIRCUNSTÂNCIA, OS AUTORES OU PROPRIETÁRIOS DE DIREITOS DE AUTOR PODERÃO SER 
RESPONSABILIZADOS POR QUAISQUER RECLAMAÇÕES, DANOS OU OUTRAS RESPONSABILIDADES, 
QUER EM ACÇÃO DE CONTRATO, DELITO OU DE OUTRA FORMA, 
DECORRENTES DE OU RELACIONADAS COM O SOFTWARE OU O USO OU OUTRAS NEGOCIAÇÕES
NO PROGRAMAS.

## Contruibuição
Voê pode dar um fork no projeto, clonar em seu PC e começar a testar, procurar por
bugs ou criar novas funcionalidades!

##### Exemplo de Contruibuição
* Bugs na documentação
* Bugs no código
* Issues existentes
* Sugestões de melhorias
* Tradução

Para realizar a contruibuição:
Podemos encontrar um erro de semântica, lógica ou mesmo se os exemplos dessa 
documentação não funcionam. A documentação pode estar desatualizada ou coisa do tipo. 
Corrija e mande o 
Pull Request.

## Autores

* Christopher Pottes
* Matheus Borges
* Rodrigo Klaes
